# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import product
from . import shipment
from . import move


def register():
    Pool.register(
        product.Product,
        move.Move,
        shipment.ShipmentIn,
        module='stock_shipment_barcode', type_='model')
    Pool.register(
        shipment.ShipmentTagsReport,
        module='stock_shipment_barcode', type_='report')

    Pool.register(
        shipment.EditionProducts,
        module='stock_shipment_barcode', type_='wizard')
