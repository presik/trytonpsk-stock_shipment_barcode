# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.report import Report
from trytond.pool import Pool, PoolMeta
from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateTransition


class ShipmentTagsReport(Report):
    'Shipment Tags Report'
    __name__ = 'stock_shipment_barcode.shipment_tags_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        lines = []
        for s in records:
            for l in s.incoming_moves:
                for x in range(int(l.number_tags)):
                    lines.append(l)

        report_context['lines'] = lines
        return report_context


class ShipmentIn(metaclass=PoolMeta):
    "Supplier Shipment"
    __name__ = 'stock.shipment.in'
    products = fields.Function(fields.Many2Many('product.product', None, None,
        'Products'), 'get_products')

    @classmethod
    def __setup__(cls):
        super(ShipmentIn, cls).__setup__()

    def get_products(self, name=None):
        return

    @classmethod
    @ModelView.button_action('stock_shipment_barcode.act_edition_products')
    def edition_products(cls, shipments):
        products_ids = []
        for shipment in shipments:
            for move in shipment.incoming_moves:
                products_ids.append(move.product.name)


class EditionProducts(Wizard):
    'Edition products'
    __name__ = 'shipment_barcode.edition_products'
    start_state = 'edition_products'
    edition_products = StateTransition()

    def transition_edition_products(self):
        pool = Pool()
        # ShipmentIn = pool.get('stock.shipment.in')
        # for shippment in ShipmentIn:
        #     for move in shippment.incoming_moves:
        #         pass
